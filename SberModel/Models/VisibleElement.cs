﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SberModel.Models
{
    public class VisibleElement : ModelListener
    {
        public List<UIElement> GraphicPresentations = new List<UIElement>();

        public virtual void UpdateView(ModelStatus status)
        {

        }

        public VisibleElement() : base() { }

    }
}
