﻿using System;
using System.Collections.Concurrent;
using SberModel.Interfaces;
using System.Windows;

namespace SberModel.Models
{

    public class Message
    {
        public IMessageUser Sender;
        public string Text;
        public ModelStatus Status;
    }

    public class NewMessageReceivedEventArgs : EventArgs
    {
        public readonly Message Message;

        public NewMessageReceivedEventArgs(Message message)
        {
            Message = message;
        }
    } 
    public class NewInterfaceElementPositionEventArgs : EventArgs 
    {
        public readonly UIElement Element;
        public readonly Point Position;

        public NewInterfaceElementPositionEventArgs(UIElement element, Point position) 
        {
            Element = element;
            Position = position;
        }
    }

    public static class Communication
    {

        public static void SendMessage(IMessageUser sender, string message)
        {
            SendMessage(new Message { Sender = sender, Text = message});
        }

        public static void SendMessage(IMessageUser sender, ModelStatus status)
        {
            SendMessage(new Message { Sender = sender, Status = status});
        }

        public static void SendMessage(Message message)
        {
            if (NewMessageReceived != null)
                NewMessageReceived(message.Sender, new NewMessageReceivedEventArgs(message));
        }

        public static void UpdateUI(ElementInfo info)
        {
            if (NewInterfacePosition != null)
                NewInterfacePosition(info.Element, new NewInterfaceElementPositionEventArgs(info.Element, info.CurrentPosition));
        }

        public static event EventHandler<NewMessageReceivedEventArgs> NewMessageReceived;
        public static event EventHandler<NewInterfaceElementPositionEventArgs> NewInterfacePosition;

    }
}
