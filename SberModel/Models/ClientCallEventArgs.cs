﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SberModel.Models
{
    public class ClientCallEventArgs : EventArgs
    {
        public readonly long Number;

        public ClientCallEventArgs(long number)
        {
            Number = number;
        }
    }
}
