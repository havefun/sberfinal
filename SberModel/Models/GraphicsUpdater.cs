﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Collections.Concurrent;
using System.Windows;
using System.Windows.Controls;

namespace SberModel.Models
{
    public class ElementInfo 
    {
        public UIElement Element { get; set; }
        public Point CurrentPosition { get; set; }
        public Point DestinationPosition { get; set; }
        public double DestinationTime { get; set; }

        public readonly double PixelsPerTimeX;
        public readonly double PixelsPerTimeY;

        public ElementInfo(UIElement element, Point destination, double time)
        {
            DestinationTime = time;
            Element = element;
            CurrentPosition = new Point(Canvas.GetLeft(element), Canvas.GetTop(element));
            DestinationPosition = destination;

            PixelsPerTimeX = (DestinationPosition.X - CurrentPosition.X) / (DestinationTime - Model.CurrentTimeInSeconds);
            PixelsPerTimeY = (DestinationPosition.Y - CurrentPosition.Y) / (DestinationTime - Model.CurrentTimeInSeconds);

        }
    }
    public class GraphicsUpdater : ModelListener
    {
        private ConcurrentQueue<ElementInfo> _elements = new ConcurrentQueue<ElementInfo>();
        private double _lastEventTime = 0.0d;
        private bool subscribed = false;
        private int _c = 0;

        public void Add(UIElement element, Point destinationPosition, double time)
        {
            _elements.Enqueue(new ElementInfo(element, destinationPosition, time));
            if (!subscribed)
            {
                _lastEventTime = Model.CurrentTimeInSeconds;
                subscribed = true;
                _c = 0;
                Subscribe();
            }
        }
        protected override void ModelTimeChanged(object sender, TimeChangedEventArgs e)
        {
            _c++;
            var c = _elements.Count;
            if (_c < 25)
                return;
            _c = 0;
            for (int i = 0; i < c; i++)
            {
                ElementInfo element = null;
                if (_elements.TryDequeue(out element))
                {
                    double elapsed = e.NewValue - _lastEventTime;
                    element.CurrentPosition = new Point(element.CurrentPosition.X + elapsed * element.PixelsPerTimeX, element.CurrentPosition.Y + elapsed * element.PixelsPerTimeY);
                    Communication.UpdateUI(element);
                    if (element.DestinationTime > e.NewValue)
                    {
                        _elements.Enqueue(element);
                    }
                    else if (element.CurrentPosition != element.DestinationPosition)
                    {
                        element.CurrentPosition = element.DestinationPosition;
                        Communication.UpdateUI(element);
                    }
                }
            }
            _lastEventTime = e.NewValue;

            if (_elements.Count == 0)
            {
                Unsubcribe();
                subscribed = false;
            }
        }
    }
}
