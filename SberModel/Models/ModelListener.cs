﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SberModel.Interfaces;

namespace SberModel.Models
{
    public abstract class ModelListener : IMessageUser, IDisposable
    {
        public long Id;

        private bool _disposed;

        public ModelListener()
        {
            Id = Model.NextId;
        }

        private double _nextEventTime;

        protected double NextEventTime
        {
            get { return _nextEventTime; }
            set { _nextEventTime = value*60; }
        }

        protected double CurrentModelTime
        {
            get { return Model.CurrentTime; }
        }

        protected void Subscribe()
        {
            Model.TimeChanged += ModelTimeChanged;
        }

        protected void Unsubcribe()
        {
            Model.TimeChanged -= ModelTimeChanged;
        }

        protected virtual void ModelTimeChanged(object sender, TimeChangedEventArgs e)
        {    
        }

        protected virtual void Dispatch()
        {
            Unsubcribe();
        }

        public virtual Type GetSenderType()
        {
            return GetType();
        }

        public void Send(string text)
        {
            Communication.SendMessage(this, text);
        }

        public void Send(string format, params object[] args)
        {
            Send(new Message { Sender = this, Status = ModelStatus.Empty, Text = string.Format(format, args) });
        }

        public void Send(ModelStatus status, string format, params object[] args)
        {
            Send(new Message { Sender = this, Status = status, Text = string.Format(format, args) });
        }

        public void Send(ModelStatus status)
        {
            Send(new Message { Sender = this, Status = status});
        }

        public void Send(ModelStatus status, string text)
        {
            Send(new Message {Sender = this, Status = status, Text = text});
        }

        public void Send(Message message)
        {
            Communication.SendMessage(message);
        }

        public void Dispose()
        {
            if (!_disposed)
            {
                _disposed = true;
                Dispatch();
            }
        }
    }
}
