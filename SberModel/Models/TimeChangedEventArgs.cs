﻿using System;

namespace SberModel.Models
{
    public class TimeChangedEventArgs : EventArgs
    {
        public readonly double LastValue;
        public readonly double NewValue;

        public TimeChangedEventArgs(double lastValue, double newValue)
        {
            LastValue = lastValue;
            NewValue = newValue;
        }
    }
}
