﻿using System;
using System.Timers;
using SberModel.Interfaces;
using SberModel.Models;
using System.ComponentModel;
using System.Windows.Input;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using System.Collections.Concurrent;

namespace SberModel
{
    public class Model : INotifyPropertyChanged
    {
        #region public fields
        public ICommand StartStopCommand { get; set; }
        public static DateTime StartingTime;
        public string ActionName
        {
            get { return _actionName; }
            set
            {
                if (_actionName != value)
                {
                    _actionName = value;
                    OnPropertyChanged("ActionName");
                }
            }
        }
        public static uint Operators
        {
            get { return _operators; }
            set
            {
                _operators = value;
                OnStaticPropertyChanged("Operators");
                OnStaticPropertyChanged("Data");
            }
        }
        public static string Time
        {
            get
            {
                int time = (int) _currentTime;
                int hours = time / 3600 + StartingTime.Hour;
                time -= 3600 * (hours - StartingTime.Hour);

                int minutes = time / 60 + StartingTime.Minute;
                time -= 60 * (minutes - StartingTime.Minute);

                int seconds = time + StartingTime.Second;
                if (seconds >= 60)
                {
                    minutes++;
                    seconds -= 60;
                }
                if (minutes >= 60)
                {
                    hours++;
                    minutes -= 60;
                }
                if (hours >= 24) hours -= 24;
                //if (minutes >= 30 )
                //    Console.WriteLine();
                return string.Format("{0}:{1}:{2}",
                    hours > 9 ? hours.ToString() : "0" + hours,
                    minutes > 9 ? minutes.ToString() : "0" + minutes,
                    seconds > 9 ? seconds.ToString() : "0" + seconds);

            }
            set
            {
                try
                {
                    int time = ValidateTime(value, true);
                    int hours = time / 3600;
                    time -= 3600 * hours;
                    int minutes = time / 60;
                    time -= 60 * minutes;
                    int seconds = time;
                    StartingTime = DateTime.Today.AddHours(hours).AddMinutes(minutes).AddSeconds(seconds);
                    OnStaticPropertyChanged("Time");

                }
                catch (Exception e)
                {
                    Debug.Write(e.ToString());
                }
            }
        }
        public static uint TimeMultiplier { get; set; }
        public static int TaskTime
        {
            get { return _taskTime; }
            set
            {
                _taskTime = value;
                OnStaticPropertyChanged("TaskTime");
                OnStaticPropertyChanged("TaskTimeString");
                OnStaticPropertyChanged("Data");
            }
        }
        public static string TaskTimeString
        {
            get
            {
                return TimeToString(TaskTime);
            }

            set
            {
                try
                {
                    var time = ValidateTime(value);
                    if (time < 60)
                        time = 60;
                    TaskTime = time;
                }
                catch { }
            }
        }
        public static int OperatorWorkTime
        {
            get { return _operatorWorkTime; }
            set
            {
                _operatorWorkTime = value;
                OnStaticPropertyChanged("OperatorWorkTime");
                OnStaticPropertyChanged("Data");
                OnStaticPropertyChanged("OperatorWorkTimeInitialString");
            }
        }
        public static string OperatorWorkTimeInitialString 
        {
            get
            {
                return TimeToString(OperatorWorkTime);
            }
            set
            {
                try
                {
                    var time = ValidateTime(value);
                    if (time < 60)
                        time = 60;
                    OperatorWorkTime = time;
                }
                catch { }
            }
        }
        public static int ClientsPerHour
        {
            get { return _clientsPerHour; }
            set
            {
                _clientsPerHour = value;
                OnStaticPropertyChanged("ClientsPerHour");
                OnStaticPropertyChanged("Data");
            }
        }
        public static IDrawer Drawer;
        public static long NextId
        {
            get
            {
                lock (_locker)
                {
                    _lastId++;
                    return _lastId;
                }
            }
        }
        public static bool IsInitialized
        {
            get { return Department != null; }
        }
        public static bool IsActive
        {
            get { return _timer != null && _timer.IsRunning; }
        }
        public static double CurrentTime
        {
            get { return _currentTime / 60; }
        }
        public static double CurrentTimeInSeconds
        {
            get { return _currentTime; }
        }
        public static Department Department { get; private set; }
        public static int ClientsInLine
        {
            get
            {
                return _clientsInLine;
            }
            set
            {
                if (_clientsInLine != value)
                {
                    _clientsInLine = value;
                    OnStaticPropertyChanged("ClientsInLine");
                }
            }
        }
        public static double ServerLoad
        {
            set 
            { 
                _serverFree += value;
                OnStaticPropertyChanged("ServerLoadPercent");
            }
        }
        public static string ServerLoadPercent
        {
            get
            {
                if (_serverFree < 0.01 || _currentTime < 0.01)
                    return "0%";
                return String.Format("{0:0.00}", (((_currentTime - _serverFree) / _currentTime) * 100)) + "%";
            }
        }
        public static string AverageWaitTime
        {
            get
            {
                int time;
                if (_waitingTime.Count == 0)
                    time = 0;
                else
                    time = Convert.ToInt32(_waitingTime.Average());
                int hours = time / 3600;
                time -= 3600 * hours;
                int minutes = time / 60;
                time -= 60 * minutes;
                int seconds = time ;

                return string.Format("{0}:{1}:{2}",
                    hours > 9 ? hours.ToString() : "0" + hours,
                    minutes > 9 ? minutes.ToString() : "0" + minutes,
                    seconds > 9 ? seconds.ToString() : "0" + seconds);
            }
        }
        public static int ClientsServed
        {
            get
            {
                return _clientsServed;
            }
            set
            {
                _clientsServed = value;
                OnStaticPropertyChanged("ClientsServed");
            }
        }
        public static GraphicsUpdater Updater
        {
            get { return _graphicsUpdater; }
        }
        public static int WorkTime { get; private set; }
        public static string WorkTimeString 
        {
            get
            {
                return TimeToString(WorkTime, true);
            }
            set
            {
                try
                {
                    WorkTime = ValidateTime(value, true);
                }
                catch { }
            }
        }
        public static List<string> XAxisChartsList
        {
            get
            {
                var variants = new List<string>
                {
                    ChartsAxisVariants.OperatorsCount, 
                    ChartsAxisVariants.ClientsPerHour, 
                    ChartsAxisVariants.OperatorWorkTime,
                    ChartsAxisVariants.ServerWorkTime 
                };
                return variants;
            }
        }
        public static List<string> YAxisChartsList
        {
            get
            {
                var variants = new List<string>
                {
                    ChartsAxisVariants.ServerLoad,
                    ChartsAxisVariants.ClientsServedPerHour,
                    ChartsAxisVariants.QueueWaitTime,
                };
                return variants;
            }
        }
        public static string XAxisCurrentSelection 
        {
            get { return _xAxisCurrentSelection; }
            set
            {
                _xAxisCurrentSelection = value;
                OnStaticPropertyChanged("XAxisCurrentSelection");
                OnStaticPropertyChanged("Data");
            }
        }
        public static string YAxisCurrentSelection
        {
            get { return _yAxisCurrentSelection; }
            set
            {
                _yAxisCurrentSelection = value;
                OnStaticPropertyChanged("YAxisCurrentSelection");
                OnStaticPropertyChanged("Data");
            }
        }
        public static List<KeyValuePair<int, double>> Data
        {
            get
            {
                double result = 0.0d;
                var data = new List<KeyValuePair<int, double>>();
                if (XAxisCurrentSelection == ChartsAxisVariants.OperatorsCount)
                    for (uint i = 1; i <= 6; i++)
                    {
                        if (YAxisCurrentSelection == ChartsAxisVariants.ServerLoad)
                            result = GetServerLoad(i, ClientsPerHour, OperatorWorkTime, TaskTime);
                        else if (YAxisCurrentSelection == ChartsAxisVariants.ClientsServedPerHour)
                            result = GetClientsServedPerHour(i, ClientsPerHour, OperatorWorkTime, TaskTime);
                        else
                            result = GetQueueWaitTime(i, ClientsPerHour, OperatorWorkTime, TaskTime);
                        data.Add(new KeyValuePair<int, double>((int) i, result));
                    }
                else if (XAxisCurrentSelection == ChartsAxisVariants.ClientsPerHour)
                    for (int i = 1; i <= 120; i++)
                    {
                        if (YAxisCurrentSelection == ChartsAxisVariants.ServerLoad)
                            result = GetServerLoad(Operators, i, OperatorWorkTime, TaskTime);
                        else if (YAxisCurrentSelection == ChartsAxisVariants.ClientsServedPerHour)
                            result = GetClientsServedPerHour(Operators, i, OperatorWorkTime, TaskTime);
                        else
                            result = GetQueueWaitTime(Operators, i, OperatorWorkTime, TaskTime);
                        data.Add(new KeyValuePair<int, double>(i, result));
                    }
                else if (XAxisCurrentSelection == ChartsAxisVariants.OperatorWorkTime)
                    for (int i = 60; i <= 600; i += 10)
                    {
                        if (YAxisCurrentSelection == ChartsAxisVariants.ServerLoad)
                            result = GetServerLoad(Operators, ClientsPerHour, i, TaskTime);
                        else if (YAxisCurrentSelection == ChartsAxisVariants.ClientsServedPerHour)
                            result = GetClientsServedPerHour(Operators, ClientsPerHour, i, TaskTime);
                        else
                            result = GetQueueWaitTime(Operators, ClientsPerHour, i, TaskTime);
                        data.Add(new KeyValuePair<int, double>(i, result));
                    }
                else
                    for (int i = 60; i <= 600; i += 10)
                    {
                        if (YAxisCurrentSelection == ChartsAxisVariants.ServerLoad)
                            result = GetServerLoad(Operators, ClientsPerHour, OperatorWorkTime, i);
                        else if (YAxisCurrentSelection == ChartsAxisVariants.ClientsServedPerHour)
                            result = GetClientsServedPerHour(Operators, ClientsPerHour, OperatorWorkTime, i);
                        else
                            result = GetQueueWaitTime(Operators, ClientsPerHour, OperatorWorkTime, i);
                        data.Add(new KeyValuePair<int, double>(i, result));
                    }
                return data;
            }
        }
        #endregion
        #region events
        public static event EventHandler<PropertyChangedEventArgs> StaticPropertyChanged;
        public event PropertyChangedEventHandler PropertyChanged;
        public static event EventHandler<TimeChangedEventArgs> TimeChanged;
        #endregion
        #region private fields
        private string _actionName;
        private static long _lastId = 0;
        private static readonly object _locker = new object();
        private static double _currentTime;
        private static int _workTime;
        private static Timer.Timer _timer;
        private static DateTime _lastTime;
        private static int _clientsInLine;
        private static double _serverFree;
        private static ConcurrentBag<double> _waitingTime;
        private static int _clientsServed;
        private static GraphicsUpdater _graphicsUpdater;
        private static string _xAxisCurrentSelection;
        private static string _yAxisCurrentSelection;
        private static uint _operators = 4;
        private static int _taskTime = 60;
        private static int _operatorWorkTime = 180;
        private static int _clientsPerHour = 35;
        #endregion

        public static void ResetModel() 
        {
            _lastId = 0;
            _currentTime = 0;
            _workTime = 8*3600;
            _timer = new Timer.Timer();
            _lastTime = DateTime.Now;
            _clientsInLine = 0;
            _serverFree = 0;
            _waitingTime = new ConcurrentBag<double>();
            _clientsServed = 0;
            _graphicsUpdater = new GraphicsUpdater();
            _yAxisCurrentSelection = ChartsAxisVariants.ServerLoad;
            _xAxisCurrentSelection = ChartsAxisVariants.OperatorsCount;
            _operators = 4;
            _taskTime = 60;
            _operatorWorkTime = 180;
            _clientsPerHour = 35;
            TimeChanged = null;

            StartingTime = DateTime.Today.AddHours(8);
            WorkTime = 10 * 3600;
            Department.Instance = null;
            TimeMultiplier = 60;
            Department = Department.Instance;

            OnStaticPropertyChanged("Time");
 
        }
        static Model()
        {
            StartingTime = DateTime.Today.AddHours(8);
            TimeMultiplier = 60;
            _currentTime = 0;
            _waitingTime = new ConcurrentBag<double>();
            _clientsServed = 0;
            _graphicsUpdater = new GraphicsUpdater();
            WorkTime = 10 * 3600;
            _yAxisCurrentSelection = ChartsAxisVariants.ServerLoad;
            _xAxisCurrentSelection = ChartsAxisVariants.OperatorsCount;
        }
        public Model()
        {
            StartStopCommand = new RelayCommand(a => StartStop());
            ActionName = "Запустить";
        }
        public static void InitializeGraphics(IDrawer drawer)
        {
            Drawer = drawer;
        }

        private static void OnStaticPropertyChanged(string propertyName)
        {
            if (StaticPropertyChanged != null)
            {
                StaticPropertyChanged(null, new PropertyChangedEventArgs(propertyName));
            }
        }
        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        private static void OnTimeChanged(TimeChangedEventArgs e)
        {
            if (TimeChanged != null)
            {
                TimeChanged(null, e);
            }
        }
        static void _timer_Elapsed(object sender, System.EventArgs  e)
        {
            DateTime currentTime = DateTime.Now;
            TimeSpan difference = currentTime - _lastTime;
            double ms = difference.TotalMilliseconds;
            var lastTime = _currentTime;
            if (ms >= 200)
                return;
            _currentTime += ms * TimeMultiplier / 1000;
            OnStaticPropertyChanged("ServerLoadPercent");
            OnStaticPropertyChanged("Time");
            _lastTime = currentTime;
            OnTimeChanged(new TimeChangedEventArgs(lastTime, _currentTime));
            bool operatorsBusy = false;
            Department.Operators.ForEach(q => 
            {
                if (q.Status != OperatorStatus.Availiable)
                    operatorsBusy = true;

            });
            if (CurrentTimeInSeconds >= WorkTime && Department.Queue.Count == 0 && !operatorsBusy)
            {
                _timer.Stop();
                Communication.SendMessage(Department, ModelStatus.GlobalTimeOver);
            }
        }


        public static void AddWaitingTimeStat(double value)
        {
            _waitingTime.Add(value);
            OnStaticPropertyChanged("AverageWaitTime");
        }
        public void Start()
        {
            Department = Department.Instance;
            _timer = new Timer.Timer();
            _timer.Mode = Timer.TimerMode.Periodic;
            _timer.Period = 1;
            _timer.Resolution = 1;
            _timer.Tick += _timer_Elapsed;
            _lastTime = DateTime.Now;
            _timer.Start();
            ActionName = "Остановить";
        }
        public void StartStop()
        {
            if (Model.IsActive)
                Pause();
            else if (CurrentTimeInSeconds <= WorkTime)
                Start();
        }
        public static void Reset()
        {
            if (Model.IsInitialized)
                _timer.Dispose();
            _timer = null;
        }
        public void Pause()
        {
            _timer.Stop();
            ActionName = "Запустить";
        }

        private static int ValidateTime(string value, bool includeHours = false)
        {
            var results = value.Split(':');
            int hours = 0;
            int minutes = 0;
            int seconds = 0;
            int offset = includeHours ? 2 : 1;
            if (results.Length > offset)
            {
                seconds = int.Parse(results[offset].Substring(0, results[offset].Length < 2 ? results[offset].Length : 2));
            }
            if (results.Length > offset - 1)
                minutes = int.Parse(results[offset - 1].Substring(0, results[offset - 1].Length < 2 ? results[offset - 1].Length : 2));
            if (includeHours)
                hours = int.Parse(results[0].Substring(0, results[0].Length < 2 ? results[0].Length : 2));
            if (minutes > 60 || seconds > 60 || hours > 60)
                throw new Exception();
            return hours * 3600 + minutes * 60 + seconds;
        }
        private static string TimeToString(int time, bool includeHours = false)
        {
            int hours = time / 3600;
            time -= 3600 * hours;
            int minutes = time / 60;
            time -= 60 * minutes;
            int seconds = time;

            if (!includeHours)
                return string.Format("{0}:{1}",
                    minutes > 9 ? minutes.ToString() : "0" + minutes,
                    seconds > 9 ? seconds.ToString() : "0" + seconds);


            return string.Format("{0}:{1}:{2}",
                hours > 9 ? hours.ToString() : "0" + hours,
                minutes > 9 ? minutes.ToString() : "0" + minutes,
                seconds > 9 ? seconds.ToString() : "0" + seconds);
        }

        private static double GetServerLoad(uint operatorsCount, int clientsPerHour, int operatorWorkTime, int serverWorkTime)
        {
            var mc = ((double)60) / clientsPerHour * 60;
            var oc = (operatorWorkTime + serverWorkTime + 20) / Math.Pow(operatorsCount, 0.9);
            var t = ((double)serverWorkTime) / (oc > mc ? oc : mc) * 100;
            return t > 100 ? 100 : t;
        }

        private static double GetClientsServedPerHour(uint operatorsCount, int clientsPerHour, int operatorWorkTime, int serverWorkTime)
        {
            var mc = ((double)60) / clientsPerHour * 60;
            var oc = (operatorWorkTime + serverWorkTime + 20) / Math.Pow(operatorsCount, 0.9);
            var tmc = 3600.0d / mc;
            var toc = 3600.0d / oc;
            return tmc < toc ? tmc : toc;
        }
        private static double GetQueueWaitTime(uint operatorsCount, int clientsPerHour, int operatorWorkTime, int serverWorkTime)
        {
            var mc = ((double)60) / clientsPerHour * 60;
            var oc = (operatorWorkTime + serverWorkTime + 20) / Math.Pow(operatorsCount, 0.7);
            var tmc = 3600 / mc;
            var toc = 3600 / oc;
            if (oc <= mc)
                return 2.0d;
            var waitTime = (oc - mc) * toc;

            return waitTime;
        }
    }

    public static class ChartsAxisVariants
    {
        public static readonly string OperatorsCount = "Количество операционистов";
        public static readonly string ClientsPerHour = "Количество клиентов в час";
        public static readonly string OperatorWorkTime = "Время работы операциониста, сек";
        public static readonly string ServerWorkTime = "Время работы сервера, сек";

        public static readonly string ServerLoad = "Загрузка сервера, %";
        public static readonly string ClientsServedPerHour = "Количество обслуженных клиентов в час";
        public static readonly string QueueWaitTime = "Время ожидания в очереди, сек";
    }
}
