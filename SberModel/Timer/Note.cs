﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SberModel.Timer
{
    public enum Key
    {
        AFlatMinor,
        EFlatMinor,
        BFlatMinor,
        FMinor,
        CMinor,
        GMinor,
        DMinor,
        AMinor,
        EMinor,
        BMinor,
        FSharpMinor,
        CSharpMinor,
        GSharpMinor,
        DSharpMinor,
        ASharpMinor,
        CFlatMajor,
        GFlatMajor,
        DFlatMajor,
        AFlatMajor,
        EFlatMajor,
        BFlatMajor,
        FMajor,
        CMajor,
        GMajor,
        DMajor,
        AMajor,
        EMajor,
        BMajor,
        FSharpMajor,
        CSharpMajor,
    }
    public enum Note
    {
        /// <summary>
        /// C natural.
        /// </summary>
        C,

        /// <summary>
        /// C sharp.
        /// </summary>
        CSharp,

        /// <summary>
        /// D flat.
        /// </summary>
        DFlat = CSharp,

        /// <summary>
        /// D natural.
        /// </summary>
        D,

        /// <summary>
        /// D sharp.
        /// </summary>
        DSharp,

        /// <summary>
        /// E flat.
        /// </summary>
        EFlat = DSharp,

        /// <summary>
        /// E natural.
        /// </summary>
        E,

        /// <summary>
        /// F natural.
        /// </summary>
        F,

        /// <summary>
        /// F sharp.
        /// </summary>
        FSharp,

        /// <summary>
        /// G flat.
        /// </summary>
        GFlat = FSharp,

        /// <summary>
        /// G natural.
        /// </summary>
        G,

        /// <summary>
        /// G sharp.
        /// </summary>
        GSharp,

        /// <summary>
        /// A flat.
        /// </summary>
        AFlat = GSharp,

        /// <summary>
        /// A natural.
        /// </summary>
        A,

        /// <summary>
        /// A sharp.
        /// </summary>
        ASharp,

        /// <summary>
        /// B flat.
        /// </summary>
        BFlat = ASharp,

        /// <summary>
        /// B natural.
        /// </summary>
        B
    }
}
