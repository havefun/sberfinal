﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SberModel
{
    interface IModelListener
    {
        void Subscribe();
    }
}
