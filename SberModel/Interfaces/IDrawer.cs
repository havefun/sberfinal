﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Shapes;

namespace SberModel.Interfaces
{
    public interface IDrawer
    {
        Line DrawLine(Point startPoint, Point endPoint);
        Rectangle DrawRectangle(Point position, int width, int height);
        Ellipse DrawEllipse(int width, int height);
        TextBlock DrawText(string text, Point position, int width, int height);
    }
}
