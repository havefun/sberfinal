﻿using SberModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SberWpf
{
    /// <summary>
    /// Логика взаимодействия для ChartsView.xaml
    /// </summary>
    public partial class ChartsView : Window
    {
        public ChartsView()
        {
            InitializeComponent();
        }

        private void DataComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string text = (sender as ComboBox).SelectedItem as string;
            string old = "";
            if (e.RemovedItems.Count != 0)
                old = e.RemovedItems[0].ToString();
            if (old == ChartsAxisVariants.OperatorsCount)
                OperatorsTextBox.IsEnabled = true;
            else if (old == ChartsAxisVariants.ClientsPerHour)
                ClientsPerHourTextBox.IsEnabled = true;
            else if (old == ChartsAxisVariants.OperatorWorkTime)
                OperatorWorkTimeTextBox.IsEnabled = true;
            else
                ServerWorkTimeTextBox.IsEnabled = true;

            if (text == ChartsAxisVariants.OperatorsCount)
                OperatorsTextBox.IsEnabled = false;
            else if (text == ChartsAxisVariants.ClientsPerHour)
                ClientsPerHourTextBox.IsEnabled = false;
            else if (text == ChartsAxisVariants.OperatorWorkTime)
                OperatorWorkTimeTextBox.IsEnabled = false;
            else
                ServerWorkTimeTextBox.IsEnabled = false;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            XDataComboBox.SelectionChanged += DataComboBox_SelectionChanged;
        }

    }
}
