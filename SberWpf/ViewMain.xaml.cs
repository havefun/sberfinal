﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Shapes;
using SberModel;
using SberModel.Models;
using System.Windows.Controls;
using System.Diagnostics;

namespace SberWpf
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        public List<Rectangle> Operators = new List<Rectangle>();

        public MainWindow()
        {
            InitializeComponent();
            Communication.NewMessageReceived += Communication_NewMessageReceived;
            Communication.NewInterfacePosition += Communication_NewInterfacePosition;
            Model.InitializeGraphics(new GraphicsContainer());
        }

        void Communication_NewInterfacePosition(object sender, NewInterfaceElementPositionEventArgs e)
        {
            ChangeElementPosition position = SetPosition;
            Dispatcher.BeginInvoke(position, e.Element, e.Position);
        }

        void Communication_NewMessageReceived(object sender, NewMessageReceivedEventArgs e)
        {
            //SetTextCallBack callBack = SetText;
            if (sender is VisibleElement)
            {
                var el = sender as VisibleElement;
                UpdateViewCallBack update = el.UpdateView;
                Dispatcher.BeginInvoke(update, e.Message.Status);
                ActWithCanvas actElement = AddElement;
                if (e.Message.Status == ModelStatus.ClientFinished)
                    actElement = RemoveElement;
                try 
                {
                    if (el.GraphicPresentations != null) {
                        foreach (var item in el.GraphicPresentations)
                        {
                            Dispatcher.BeginInvoke(actElement, item);
                        }
                    }
                }
                catch (Exception exception)
                {
                    Debug.Write(exception.ToString());
                }
            }
            if (sender is Department)
            {
                if (e.Message.Status == ModelStatus.GlobalTimeOver)
                {
                    Dispatcher.BeginInvoke(new Action(() => MessageBox.Show("Рабочий день подошел к концу")));
                }
            }
            //Dispatcher.BeginInvoke(callBack,
            //    string.Format("{0}: {1}{2}", DateTime.Now, e.Message.Text,
            //        Environment.NewLine));
        }
        private void SetPosition(UIElement element, Point point)
        {
            if (Grid.Children.Contains(element))
            {
                Canvas.SetLeft(element, point.X);
                Canvas.SetTop(element, point.Y);
            }
        }

        private void AddElement(UIElement element)
        {
            if (element != null && !Grid.Children.Contains(element))
                Grid.Children.Add(element);
        }

        private void RemoveElement(UIElement element)
        {
            if (Grid.Children.Contains(element))
                Grid.Children.Remove(element);
        }
        private void buttonStart_Click(object sender, RoutedEventArgs e)
        {
            ResetButton.IsEnabled = !ResetButton.IsEnabled;
            OperatorsTextBox.IsEnabled = false;
            TimeTextBox.IsEnabled = false;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Model.Reset();
            Communication.NewMessageReceived -= Communication_NewMessageReceived;
        }

        private void Button_Charts(object sender, RoutedEventArgs e)
        {
            var charts = new ChartsView();
            charts.Show();
        }

        private void Button_Reset(object sender, RoutedEventArgs e)
        {
            OperatorsTextBox.IsEnabled = true;
            TimeTextBox.IsEnabled = true;
            Model.ResetModel();
            Grid.Children.Clear();
        }
    }

    internal delegate void SetTextCallBack(string text);

    internal delegate void UpdateViewCallBack(ModelStatus status);

    internal delegate void ActWithCanvas(UIElement element);

    internal delegate void ChangeElementPosition(UIElement element, Point point);
}
